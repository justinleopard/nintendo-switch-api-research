const redirectURL = ''

function extractSessionTokenCode(redirectURL) {
    const params = {};
    redirectURL.split('#')[1]
        .split('&')
        .forEach(str => {
            const splitStr = str.split('=');
            params[splitStr[0]] = splitStr[1];
        });
    return params.session_token_code;
}

const session_token_code = extractSessionTokenCode(redirectURL);
getSessionToken(session_token_code, codeVerifier)
    .then(session_token => {
        // Use the session_token here
        console.log(session_token);
    })
    .catch(error => {
        // Handle the error here
        console.error(error);
    });

const request2 = require('request-promise-native');
const jar = request2.jar();
const request = request2.defaults({ jar: jar });

const userAgentVersion = `1.9.0`; // version of Nintendo Switch App, updated once or twice per year

async function getSessionToken(session_token_code, codeVerifier) {
  const resp = await request({
    method: 'POST',
    uri: 'https://accounts.nintendo.com/connect/1.0.0/api/session_token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Platform': 'Android',
      'X-ProductVersion': userAgentVersion,
      'User-Agent': `OnlineLounge/${userAgentVersion} NASDKAPI Android`
    },
    form: {
      client_id: '71b963c1b7b6d119',
      session_token_code: session_token_code,
      session_token_code_verifier: codeVerifier
    },
    json: true
  });

  return resp.session_token;
}
