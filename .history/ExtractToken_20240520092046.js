const redirectURL = 'npf71b963c1b7b6d119://auth#session_token_code=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTYyMDk3MTEsInN1YiI6IjY1NTViZTcyZmUzM2ZkNzciLCJpYXQiOjE3MTYyMDkxMTEsInN0YzptIjoiUzI1NiIsInN0YzpzY3AiOlswLDgsOSwxNywyM10sInR5cCI6InNlc3Npb25fdG9rZW5fY29kZSIsImp0aSI6Ijk3NTExODIyOTQ2IiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5uaW50ZW5kby5jb20iLCJzdGM6YyI6IjI5eGd1bnZ1RElSYXNoYXRRYnkwd2tadURSc3VUR01oLXlOb1plQndaeGciLCJhdWQiOiI3MWI5NjNjMWI3YjZkMTE5In0.90_9lRK7qC-r2wqyUjGlTAemgkCvfQYoIobFllUakRw&state=fgDWwlcuEVjsH2FlaH7x-ya2nqjWMa9nG_6NkhigIB4qnOaM&session_state=f0a177d2ad2740f7a1c66cc499867b0490ac5bc27fe5bd5c147f216bcbc30fd7';

function extractSessionTokenCode(redirectURL) {
    const params = {};
    redirectURL.split('#')[1]
        .split('&')
        .forEach(str => {
            const splitStr = str.split('=');
            params[splitStr[0]] = splitStr[1];
        });
    return params.session_token_code;
}

const session_token_code = extractSessionTokenCode(redirectURL);
getSessionToken(session_token_code, codeVerifier)
    .then(session_token => {
        // Use the session_token here
        console.log(session_token);
    })
    .catch(error => {
        // Handle the error here
        console.error(error);
    });

const request2 = require('request-promise-native');
const jar = request2.jar();
const request = request2.defaults({ jar: jar });

const userAgentVersion = `1.9.0`; // version of Nintendo Switch App, updated once or twice per year

async function getSessionToken(session_token_code, codeVerifier) {
  const resp = await request({
    method: 'POST',
    uri: 'https://accounts.nintendo.com/connect/1.0.0/api/session_token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Platform': 'Android',
      'X-ProductVersion': userAgentVersion,
      'User-Agent': `OnlineLounge/${userAgentVersion} NASDKAPI Android`
    },
    form: {
      client_id: '71b963c1b7b6d119',
      session_token_code: session_token_code,
      session_token_code_verifier: codeVerifier
    },
    json: true
  });

  return resp.session_token;
}
