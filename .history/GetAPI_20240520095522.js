const redirectURL = 'npf71b963c1b7b6d119://auth#session_token_code=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTYyMDk3MTEsInN1YiI6IjY1NTViZTcyZmUzM2ZkNzciLCJpYXQiOjE3MTYyMDkxMTEsInN0YzptIjoiUzI1NiIsInN0YzpzY3AiOlswLDgsOSwxNywyM10sInR5cCI6InNlc3Npb25fdG9rZW5fY29kZSIsImp0aSI6Ijk3NTExODIyOTQ2IiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5uaW50ZW5kby5jb20iLCJzdGM6YyI6IjI5eGd1bnZ1RElSYXNoYXRRYnkwd2tadURSc3VUR01oLXlOb1plQndaeGciLCJhdWQiOiI3MWI5NjNjMWI3YjZkMTE5In0.90_9lRK7qC-r2wqyUjGlTAemgkCvfQYoIobFllUakRw&state=fgDWwlcuEVjsH2FlaH7x-ya2nqjWMa9nG_6NkhigIB4qnOaM&session_state=f0a177d2ad2740f7a1c66cc499867b0490ac5bc27fe5bd5c147f216bcbc30fd7';

function extractSessionTokenCode(redirectURL) {
    const params = {};
    redirectURL.split('#')[1]
        .split('&')
        .forEach(str => {
            const splitStr = str.split('=');
            params[splitStr[0]] = splitStr[1];
        });
    return params.session_token_code;
}



const request2 = require('request-promise-native');
const jar = request2.jar();
const request = request2.defaults({ jar: jar });

const userAgentVersion = `2.10.0`; // version of Nintendo Switch App, updated once or twice per year

async function getSessionToken(session_token_code, codeVerifier) {

  const resp = await request({
    method: 'POST',
    uri: 'https://accounts.nintendo.com/connect/1.0.0/api/session_token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Platform': 'Android',
      'X-ProductVersion': userAgentVersion,
      'User-Agent': `OnlineLounge/${userAgentVersion} NASDKAPI Android`
    },
    form: {
      client_id: '71b963c1b7b6d119',
      session_token_code: session_token_code,
      session_token_code_verifier: codeVerifier
    },
    json: true
  });

  return resp.session_token;
}

const { v4: uuidv4 } = require('uuid');

async function getWebServiceTokenWithSessionToken(sessionToken, game) {
    const apiTokens = await getApiToken(sessionToken); // I. Get API Token
    const userInfo = await getUserInfo(apiTokens.access); // II. Get userInfo

    const guid = uuidv4();
    const timestamp = String(Math.floor(Date.now() / 1000));

    const flapg_nso = await callFlapg(apiTokens.id, guid, timestamp, "nso"); // III. Get F flag [NSO]
    const apiAccessToken = await getApiLogin(userInfo, flapg_nso); // IV. Get API Access Token
    const flapg_app = await callFlapg(apiAccessToken, guid, timestamp, "app"); // V. Get F flag [App]
    const web_service_token =  await getWebServiceToken(apiAccessToken, flapg_app, game); // VI. Get Web Service Token
    return web_service_token;
  }


  const userAgentString = `com.nintendo.znca/${userAgentVersion} (Android/7.1.2)`;

async function getApiToken(session_token) {
    const resp = await request({
        method: 'POST',
        uri: 'https://accounts.nintendo.com/connect/1.0.0/api/token',
        headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'X-Platform': 'Android',
        'X-ProductVersion': userAgentVersion,
        'User-Agent': userAgentString
        },
        json: {
        client_id: '71b963c1b7b6d119',
        grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer-session-token',
        session_token: session_token
        }
    });

    return {
        id: resp.id_token,
        access: resp.access_token
    };
}

async function getHash(idToken, timestamp) {
  const response = await request({
    method: 'POST',
    uri: 'https://elifessler.com/s2s/api/gen2',
    headers: {
      'User-Agent': `yournamehere` // your unique id here
    },
    form: {
      naIdToken: idToken,
      timestamp: timestamp
    }
  });

  const responseObject = JSON.parse(response);
  return responseObject.hash;
}

async function callFlapg(idToken, guid, timestamp, login) {
    const hash = await getHash(idToken, timestamp)
    const response = await request({
        method: 'GET',
        uri: 'https://flapg.com/ika2/api/login?public',
        headers: {
        'x-token': idToken,
        'x-time': timestamp,
        'x-guid': guid,
        'x-hash': hash,
        'x-ver': '3',
        'x-iid': login
        }
    });
    const responseObject = JSON.parse(response);

    return responseObject.result;
}

async function getUserInfo(token) {
const response = await request({
    method: 'GET',
    uri: 'https://api.accounts.nintendo.com/2.0.0/users/me',
    headers: {
    'Content-Type': 'application/json; charset=utf-8',
    'X-Platform': 'Android',
    'X-ProductVersion': userAgentVersion,
    'User-Agent': userAgentString,
    Authorization: `Bearer ${token}`
    },
    json: true
});

return {
    nickname: response.nickname,
    language: response.language,
    birthday: response.birthday,
    country: response.country
};
}

async function getApiLogin(userinfo, flapg_nso) {
    const resp = await request({
        method: 'POST',
        uri: 'https://api-lp1.znc.srv.nintendo.net/v1/Account/Login',
        headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'X-Platform': 'Android',
        'X-ProductVersion': userAgentVersion,
        'User-Agent': userAgentString,
        Authorization: 'Bearer'
        },
        body: {
        parameter: {
            language: userinfo.language,
            naCountry: userinfo.country,
            naBirthday: userinfo.birthday,
            f: flapg_nso.f,
            naIdToken: flapg_nso.p1,
            timestamp: flapg_nso.p2,
            requestId: flapg_nso.p3
        }
        },
        json: true,
        gzip: true
    });
    return resp.result.webApiServerCredential.accessToken;
}


async function getWebServiceToken(token, flapg_app, game) {
  let parameterId;
    if (game == 'S2') {
      parameterId = 5741031244955648; // SplatNet 2 ID
    } else if (game == 'AC') {
      parameterId = 4953919198265344; // Animal Crossing ID
    }
  const resp = await request({
    method: 'POST',
    uri: 'https://api-lp1.znc.srv.nintendo.net/v2/Game/GetWebServiceToken',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'X-Platform': 'Android',
      'X-ProductVersion': userAgentVersion,
      'User-Agent': userAgentString,
      Authorization: `Bearer ${token}`
    },
    json: {
      parameter: {
        id: parameterId,
        f: flapg_app.f,
        registrationToken: flapg_app.p1,
        timestamp: flapg_app.p2,
        requestId: flapg_app.p3
      }
    }
  });

  return {
    accessToken: resp.result.accessToken,
    expiresAt: Math.round(new Date().getTime()) + resp.result.expiresIn
  };
}


(async () => {
    const sessionToken = await getSessionToken(params.session_token_code, authParams.codeVerifier);
    const webServiceToken = await getWebServiceTokenWithSessionToken(sessionToken, game='S2');
    console.log('Web Service Token', webServiceToken);
})()
